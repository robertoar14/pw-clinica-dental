<?php
/**
 * Template Name: Servicios
 *
 * Template para visualización de servicios
 *
 * @package Lodestar
 */
get_header();
?>

<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <article id="post-5" class="post-5 page type-page status-publish hentry">
                <header class="entry-header">
                    <h1 class="entry-title">Servicios</h1>
                </header>
                <div class="entry-content">
                    <figure class="wp-block-gallery columns-2 is-cropped">
                        <ul class="blocks-gallery-grid">
                            <li class="blocks-gallery-item">
                                <figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones.jpg"
                                        alt="" data-id="6"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones.jpg"
                                        data-link="/?attachment_id=6" class="wp-image-6"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones.jpg 850w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones-300x200.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones-768x512.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones-700x467.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Extracciones-600x400.jpg 600w"
                                        sizes="(max-width: 850px) 100vw, 850px">
                                    <figcaption class="blocks-gallery-item__caption">Extracciones</figcaption>
                                </figure>
                            </li>
                            <li class="blocks-gallery-item">
                                <figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza.jpg" alt=""
                                        data-id="7"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza.jpg"
                                        data-link="/?attachment_id=7" class="wp-image-7"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza.jpg 1000w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-300x200.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-768x512.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-700x467.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-900x600.jpg 900w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-600x400.jpg 600w"
                                        sizes="(max-width: 1000px) 100vw, 1000px">
                                    <figcaption class="blocks-gallery-item__caption">Limpieza</figcaption>
                                </figure>
                            </li>
                            <li class="blocks-gallery-item">
                                <figure><img
                                        src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-1024x538.jpg"
                                        alt="" data-id="8"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2.jpg"
                                        data-link="/?attachment_id=8" class="wp-image-8"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-1024x538.jpg 1024w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-300x158.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-768x403.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-700x368.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2-762x400.jpg 762w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Limpieza-2.jpg 1200w"
                                        sizes="(max-width: 1024px) 100vw, 1024px">
                                    <figcaption class="blocks-gallery-item__caption">Blanqueamientos</figcaption>
                                </figure>
                            </li>
                            <li class="blocks-gallery-item">
                                <figure><img
                                        src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General.jpg"
                                        alt="" data-id="9"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General.jpg"
                                        data-link="/?attachment_id=9" class="wp-image-9"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General.jpg 950w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General-300x169.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General-768x433.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General-700x394.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontologia-General-710x400.jpg 710w"
                                        sizes="(max-width: 950px) 100vw, 950px">
                                    <figcaption class="blocks-gallery-item__caption">Odontología general</figcaption>
                                </figure>
                            </li>
                            <li class="blocks-gallery-item">
                                <figure><img
                                        src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-1024x683.jpg"
                                        alt="" data-id="10"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria.jpg"
                                        data-link="/?attachment_id=10" class="wp-image-10"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-1024x683.jpg 1024w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-300x200.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-768x512.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-1536x1024.jpg 1536w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-2048x1366.jpg 2048w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-700x467.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-2000x1334.jpg 2000w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-900x600.jpg 900w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Odontopediatria-600x400.jpg 600w"
                                        sizes="(max-width: 1024px) 100vw, 1024px">
                                    <figcaption class="blocks-gallery-item__caption">Odontopediatría</figcaption>
                                </figure>
                            </li>
                            <li class="blocks-gallery-item">
                                <figure><img
                                        src="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales.jpg"
                                        alt="" data-id="11"
                                        data-full-url="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales.jpg"
                                        data-link="/?attachment_id=11" class="wp-image-11"
                                        srcset="<?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales.jpg 1024w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales-300x169.jpg 300w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales-768x434.jpg 768w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales-700x395.jpg 700w, <?php echo get_template_directory_uri(); ?>/assets/images/servicios/Tratamientos-dentales-709x400.jpg 709w"
                                        sizes="(max-width: 1024px) 100vw, 1024px">
                                    <figcaption class="blocks-gallery-item__caption">Tratamientos dentales</figcaption>
                                </figure>
                            </li>
                        </ul>
                    </figure>
                </div>
                <footer class="entry-footer">
                </footer>
            </article><!-- #post-## -->
        </main>
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php
get_footer();
