
<?php
/*
Template Name: Página prueba
*/

get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">            
<!-- wp:heading {"level":4} -->
<h4>Servicios</h4>
<!-- /wp:heading -->

<!-- wp:image {"id":67,"align":"left","width":255,"height":169} -->
<div class="wp-block-image"><figure class="alignleft is-resized"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Index/dentistas.jpg" alt="" class="wp-image-67" width="255" height="169"/></figure></div>
<!-- /wp:image -->

<!-- wp:image {"id":68,"width":285,"height":172} -->
<figure class="wp-block-image is-resized"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Index/dentista-580x350.jpg" alt="" class="wp-image-68" width="285" height="172"/></figure>
<!-- /wp:image -->

<!-- wp:image {"id":70,"align":"left","width":253,"height":185} -->
<div class="wp-block-image"><figure class="alignleft is-resized"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Index/Captura-de-pantalla-11291.png" alt="" class="wp-image-70" width="253" height="185"/></figure></div>
<!-- /wp:image -->

<!-- wp:image {"id":71,"width":290,"height":200} -->
<figure class="wp-block-image is-resized"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Index/2-Diagnosticos_3D_e_implantes_con_carga_inmediata-clinica_dental_O2-dentiasta_alicante.jpg" alt="" class="wp-image-71" width="290" height="200"/></figure>
<!-- /wp:image -->

<!-- wp:heading {"level":4} -->
<h4>Acerca de</h4>
<!-- /wp:heading -->

<!-- wp:html -->
<div class="wp-block-image"><figure class="aligncenter is-resized"><figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Index/dentistas-espana.jpg" alt="" class="wp-image-72" width="1200" height="799"></figure></figure></div>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center">¡Qué tal! soy el doctor José Alfonso Castillo Anleu, soy cirujano dentista, graduado de la universidad de San Carlos de Guatemala y tengo ya más de 25 años de experiencia en todo lo relacionado a lo que estas buscando.</p>
<!-- /wp:paragraph -->

		</main>
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->
<?php
get_footer();
