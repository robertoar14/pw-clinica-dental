<?php
/*
Template Name: Información Dogtor
*/

get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<!-- wp:paragraph -->
<p>¡Qué tal!, soy el Doctor José Alfonso Castillo Anleu, soy Cirujano Dentista, graduado de la Universidad de San Carlos de Guatemala y tengo ya más de 25 años de experiencia, en todo lo relacionado a lo que estás buscando.</p>
<!-- /wp:paragraph -->

<!-- wp:media-text {"mediaId":22,"mediaType":"image"} -->
<div class="wp-block-media-text alignwide"><figure class="wp-block-media-text__media"><img src="http://localhost/wordpress/wp-content/uploads/2019/11/dentista1-1.jpg" alt="" class="wp-image-22"/></figure><div class="wp-block-media-text__content"><!-- wp:paragraph {"placeholder":"Content…"} -->
<p>Hago todo tipo de servicios para darle un sentido más importante a tu sonrisa, si quieres conocerlos visita mi página de servicios.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text -->

<!-- wp:media-text {"customBackgroundColor":"#b7edd8","mediaPosition":"right","mediaId":23,"mediaType":"image"} -->
<div class="wp-block-media-text alignwide has-media-on-the-right" style="background-color:#b7edd8"><figure class="wp-block-media-text__media"><img src="http://localhost/wordpress/wp-content/uploads/2019/11/dentista2.jpg" alt="" class="wp-image-23"/></figure><div class="wp-block-media-text__content"><!-- wp:paragraph {"placeholder":"Content…"} -->
<p>Otra de mis profesiones al igual que de mis pasiones es el ser docente universitario. Me encanta compartir todo lo que sé y formar a futuros profesionales que estén bien preparados.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text -->

<!-- wp:image {"id":28,"width":900,"height":350} -->
<figure class="wp-block-image is-resized"><img src="http://localhost/wordpress/wp-content/uploads/2019/11/dentista3-1.jpg" alt="" class="wp-image-28" width="900" height="350"/><figcaption>He atendido a todo tipo de generación de personas, desde niños hasta personas de la tercerda de edad, los he apoyado en sus procesos de caries, canales, extracciones, entre otras acciones.</figcaption></figure>
<!-- /wp:image -->

<!-- wp:quote {"align":"right","className":"is-style-default"} -->
<blockquote style="text-align:right" class="wp-block-quote is-style-default"><p><strong>Colegio Estomatológico de Guatemala</strong></p><cite>El colegio al que mi profesión pertenece es al anteriormente mencionado y tengo el colegiado activo 1208, siendo yo uno de los primeros cirujanos dentistas en Guatemala, pero sobre todo en Quetzaltenango.</cite></blockquote>
<!-- /wp:quote -->


		</main>
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->
<?php
get_footer();
