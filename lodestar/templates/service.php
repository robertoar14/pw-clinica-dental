<?php /* Template Name: Servicio */ 
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lodestar
 */
wp_enqueue_style( 'service', get_template_directory_uri() . '/css/service.css',false,'1.1','all');
get_header(); ?>

<div class="wrap">
	<div class="full">
		<div id="primary" class="full">
			<main id="main" class="site-main" role="main">
				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'components/service/simple');
				endwhile; // End of the loop.
				?>
			</main>
		</div><!-- #primary -->
	</div>
</div><!-- .wrap -->
<?php
get_footer();