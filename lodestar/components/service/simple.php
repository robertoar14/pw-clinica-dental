<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lodestar
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="wrapper">
            <?php
                echo '<img src="./wp-content/themes/lodestar/assets/images/',the_title(),'.jpg" height="42" width="42">';
            ?>
	<header class="entry-header">
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
	</header>
	<div class="entry-content">
        </div>
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'lodestar' ),
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>'
			) );
		?>
	</div>
	<footer class="entry-footer">
		<?php lodestar_edit_link( get_the_ID() ); ?>
	</footer>
</article><!-- #post-## -->
